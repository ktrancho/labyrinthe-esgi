CC= gcc
CFLAGS= -Wall -O2
CLIBS= -lSDL -lSDL_ttf -lSDL_gfx -lSDL_image
EXE= labyrintheESGI
OBJ= app/obj/
SRC= app/sources/
INCL= app/includes/
FILEC:= $(wildcard $(SRC)*.c)
FILEH:= $(wildcard $(INCL)*.c)
FILEO:= $(patsubst $(SRC)%.c,$(OBJ)%.o,$(FILEC))

$(EXE): $(FILEO)
	$(CC) -o $(EXE) $^ $(CFLAGS) $(CLIBS)

$(OBJ)main.o : $(SRC)main.c $(FILEH)
	@if [ ! -d "$(OBJ)" ]; then mkdir $(OBJ); fi;
	$(CC) -o $@ -c $< $(CFLAGS)

$(OBJ)%.o: $(SRC)%.c $(INCL)%.h
	@if [ ! -d "$(OBJ)" ]; then mkdir $(OBJ); fi;
	$(CC) -o $@ -c $< $(CFLAGS)

clean:
	rm -f $(EXE)
	rm -f log
	if [ -d "$(OBJ)" ]; then rm -f $(OBJ)*.o; rmdir $(OBJ); fi;

