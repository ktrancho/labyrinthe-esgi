#include "../includes/camera.h"

#include <SDL/SDL_rotozoom.h>
#include <SDL/SDL_gfxPrimitives.h>

Camera creer_camera(int x, int y, int dx, int dy, int distance, int niveau, Grille grille) {

	Camera camera = {x, y, dx, dy, distance, niveau, 0, grille};
	return camera;
}

void camera_set_position(Camera * camera, int x, int y, int dx, int dy) {
	camera->x = x;
	camera->y = y;
	camera->dx = dx;
	camera->dy = dy;
}

/*static void displayTile(Camera * camera, int x, int y, float scale, int r, int g, int b) {
	SDL_Rect displayBloc;
	float minDim = (window_window.width < window_window.height) ? window_window.width : window_window.height;
	displayBloc.x = window_window.width / 2 + (minDim / 2) * (float)(x - .5 + (1 - scale) / 2.) / camera->distance;
	displayBloc.y = window_window.height / 2 + (minDim / 2) * (float)(y - .5 + (1 - scale) / 2.) / camera->distance;
	displayBloc.w = scale * (minDim / 2) / (float)(camera->distance);
	displayBloc.h = scale * (minDim / 2) / (float)(camera->distance);
	SDL_FillRect(window_window.screen, &displayBloc, SDL_MapRGB(window_window.screen->format, r, g, b));
}*/

static void displayVoid(Camera * camera, int x, int y, float scale, Grille grille) {
	int env = (camera->niveau / NB_PERSOS) % NB_DECORS;
	SDL_Rect displayBloc;
	float minDim = (window_window.width < window_window.height) ? window_window.width : window_window.height;
	displayBloc.x = window_window.width / 2 + (minDim / 2) * (float)(x - .5 + (1 - scale) / 2.) / camera->distance;
	displayBloc.y = window_window.height / 2 + (minDim / 2) * (float)(y - .5 + (1 - scale) / 2.) / camera->distance;
	displayBloc.w = scale * (minDim / 2) / (float)(camera->distance);
	displayBloc.h = scale * (minDim / 2) / (float)(camera->distance);
	SDL_Rect selectionImage;
	selectionImage.x = 100;
	selectionImage.y = 50;
	selectionImage.h = 50;
	selectionImage.w = 50;
	SDL_BlitSurface(window_window.decors[env], &selectionImage, window_window.screen, &displayBloc);
}

static int directionToPersoId(int dx, int dy) {
	if(dx > 0) return 1;
	else if(dx < 0) return 3;
	else if(dy < 0) return 2;
	return 0;
}

static void displayPerso(Camera * camera, int id, int x, int y, float scale, int direction) {
	SDL_Rect displayBloc;
	float minDim = (window_window.width < window_window.height) ? window_window.width : window_window.height;
	displayBloc.x = window_window.width / 2 + (minDim / 2) * (float)(x - .5 + (1 - scale) / 2.) / camera->distance;
	displayBloc.y = window_window.height / 2 + (minDim / 2) * (float)(y - .5 + (1 - scale) / 2.) / camera->distance;
	displayBloc.w = scale * (minDim / 2) / (float)(camera->distance);
	displayBloc.h = scale * (minDim / 2) / (float)(camera->distance);
	SDL_Rect selectionImage;
	displayVoid(camera, x, y, scale, NULL);
	selectionImage.x = 50 * direction;
	selectionImage.y = 0;
	selectionImage.h = 50;
	selectionImage.w = 50;
	SDL_BlitSurface(window_window.persos[id], &selectionImage, window_window.screen, &displayBloc);
}

static void displayWall(Camera * camera, int x, int y, float scale, Grille grille) {
	int env = (camera->niveau / NB_PERSOS) % NB_DECORS;
	SDL_Rect displayBloc;
	float minDim = (window_window.width < window_window.height) ? window_window.width : window_window.height;
	/*displayBloc.w = scale * (minDim / 2) / (float)(camera->distance);
	displayBloc.h = scale * (minDim / 2) / (float)(camera->distance);*/
	int bx, by;
	int cx = camera->x + x, cy = camera->y + y;
	displayVoid(camera, x, y, scale, grille);
	/* TODO : une boucle pour les directions */
	SDL_Rect selectionImage;
	/* haut gauche */
	selectionImage.x = 0;
	selectionImage.y = 0;
	selectionImage.h = 25;
	selectionImage.w = 25;
	bx = (!grille_case(grille, cx - 1, cy) || *grille_case(grille, cx - 1, cy) == GRILLE_ITEM_WALL) ? 1 : 0;
	by = (!grille_case(grille, cx, cy - 1) || *grille_case(grille, cx, cy - 1) == GRILLE_ITEM_WALL) ? 1 : 0;
	if(bx && by && (!grille_case(grille, cx - 1, cy - 1) || *grille_case(grille, cx - 1, cy - 1) == GRILLE_ITEM_WALL)) {
		selectionImage.x += 100;
	} else {
		selectionImage.x += 50 * by;
		selectionImage.y += 50 * bx;
	}
	displayBloc.x = window_window.width / 2 + (minDim / 2) * (float)(x - .5 + (1 - scale) / 2.) / camera->distance;
	displayBloc.y = window_window.height / 2 + (minDim / 2) * (float)(y - .5 + (1 - scale) / 2.) / camera->distance;
	SDL_BlitSurface(window_window.decors[env], &selectionImage, window_window.screen, &displayBloc);
	/* haut droite */
	selectionImage.x = 25;
	selectionImage.y = 0;
	selectionImage.h = 25;
	selectionImage.w = 25;
	bx = (!grille_case(grille, cx + 1, cy) || *grille_case(grille, cx + 1, cy) == GRILLE_ITEM_WALL) ? 1 : 0;
	by = (!grille_case(grille, cx, cy - 1) || *grille_case(grille, cx, cy - 1) == GRILLE_ITEM_WALL) ? 1 : 0;
	if(bx && by && (!grille_case(grille, cx + 1, cy - 1) || *grille_case(grille, cx + 1, cy - 1) == GRILLE_ITEM_WALL)) {
		selectionImage.x += 100;
	} else {
		selectionImage.x += 50 * by;
		selectionImage.y += 50 * bx;
	}
	displayBloc.x = 25 + window_window.width / 2 + (minDim / 2) * (float)(x - .5 + (1 - scale) / 2.) / camera->distance;
	displayBloc.y = window_window.height / 2 + (minDim / 2) * (float)(y - .5 + (1 - scale) / 2.) / camera->distance;
	SDL_BlitSurface(window_window.decors[env], &selectionImage, window_window.screen, &displayBloc);
	/* bas gauche */
	selectionImage.x = 0;
	selectionImage.y = 25;
	selectionImage.h = 25;
	selectionImage.w = 25;
	bx = (!grille_case(grille, cx - 1, cy) || *grille_case(grille, cx - 1, cy) == GRILLE_ITEM_WALL) ? 1 : 0;
	by = (!grille_case(grille, cx, cy + 1) || *grille_case(grille, cx, cy + 1) == GRILLE_ITEM_WALL) ? 1 : 0;
	if(bx && by && (!grille_case(grille, cx - 1, cy + 1) || *grille_case(grille, cx - 1, cy + 1) == GRILLE_ITEM_WALL)) {
		selectionImage.x += 100;
	} else {
		selectionImage.x += 50 * by;
		selectionImage.y += 50 * bx;
	}
	displayBloc.x = window_window.width / 2 + (minDim / 2) * (float)(x - .5 + (1 - scale) / 2.) / camera->distance;
	displayBloc.y = 25 + window_window.height / 2 + (minDim / 2) * (float)(y - .5 + (1 - scale) / 2.) / camera->distance;
	SDL_BlitSurface(window_window.decors[env], &selectionImage, window_window.screen, &displayBloc);
	/* bas droite */
	selectionImage.x = 25;
	selectionImage.y = 25;
	selectionImage.h = 25;
	selectionImage.w = 25;
	bx = (!grille_case(grille, cx + 1, cy) || *grille_case(grille, cx + 1, cy) == GRILLE_ITEM_WALL) ? 1 : 0;
	by = (!grille_case(grille, cx, cy + 1) || *grille_case(grille, cx, cy + 1) == GRILLE_ITEM_WALL) ? 1 : 0;
	if(bx && by && (!grille_case(grille, cx + 1, cy + 1) || *grille_case(grille, cx + 1, cy + 1) == GRILLE_ITEM_WALL)) {
		selectionImage.x += 100;
	} else {
		selectionImage.x += 50 * by;
		selectionImage.y += 50 * bx;
	}
	displayBloc.x = 25 + window_window.width / 2 + (minDim / 2) * (float)(x - .5 + (1 - scale) / 2.) / camera->distance;
	displayBloc.y = 25 + window_window.height / 2 + (minDim / 2) * (float)(y - .5 + (1 - scale) / 2.) / camera->distance;
	SDL_BlitSurface(window_window.decors[env], &selectionImage, window_window.screen, &displayBloc);
	/*SDL_FillRect(window_window.screen, &displayBloc, SDL_MapRGB(window_window.screen->format, r, g, b));*/
}

static void displayRoundTile(Camera * camera, int x, int y, float scale, int r, int g, int b) {
	SDL_Rect displayBloc;
	float minDim = (window_window.width < window_window.height) ? window_window.width : window_window.height;
	displayBloc.x = window_window.width / 2 + (minDim / 2) * (float)(x - .5 + (1 - scale) / 2.) / camera->distance;
	displayBloc.y = window_window.height / 2 + (minDim / 2) * (float)(y - .5 + (1 - scale) / 2.) / camera->distance;
	float rad = 0.5 * scale * (minDim / 2) / (float)(camera->distance);
	filledCircleRGBA(window_window.screen, displayBloc.x + rad, displayBloc.y + rad, rad, r, g, b, 255);
}

static int max(int a, int b) {
	return (a > b) ? a : b;
}

static int min(int a, int b) {
	return (a < b) ? a : b;
}

void camera_afficher(Camera * camera) {
	
	/*int offset_x = 40 - camera->distance;
	int offset_y = 12 - camera->distance;*/
	int x, y;
	Grille grille = camera->looking_grille;
	GrilleElement * current;
	char currentChar;
	float lum;
	for(y = -camera->distance; y <= camera->distance; ++y) {
		for(x = -camera->distance; x <= camera->distance; ++x) {
			current = grille_case(grille, camera->x + x, camera->y + y);
			currentChar = (current != NULL) ? *current : GRILLE_ITEM_WALL;
			/*lum = 1.1 - (x * x + y * y) / (0.8 * camera->distance * camera->distance);
			lum = (lum > 1) ? 1 : ((lum < 0) ? 0 : lum);*/
			lum = 1;
			if(max(x, y) <= 1. * camera->distance) {
				switch(currentChar) {
					case GRILLE_ITEM_WALL   : displayWall(camera, x, y, 1., grille); break;
					case GRILLE_ITEM_VOID   : displayVoid(camera, x, y, 1., grille); break;
					case GRILLE_ITEM_EXIT   : displayPerso(camera, (camera->niveau + 1) % NB_PERSOS, x, y, 1., 0); break;
					default : break;
				}
				/*TODO : mvprintw(offset_y + y, offset_x + x, "%c", currentChar);*/
			} else if(x * x + y * y <= camera->distance * camera->distance) {
				/*TODO : mvprintw(offset_y + y, offset_x + x, "~");*/
			}
			
		}
	}
	x = camera->x; y = camera->y;
	int d;
	int dx, dy;
	int min_dx = x, min_dy = y;
	GrilleElement * currentElement = NULL;
	long * currentDM = NULL;
	long * minDM = NULL;
	long lastDistance;
	long currentDistance = *grille_distance_case(grille, x, y);
	/*TODO : mvprintw(1, 1, "taille : %d x %d", grille->largeur, grille->hauteur);*/
	/*TODO : mvprintw(2, 1, "sortie à %ld pas", currentDistance);*/
	int steps = 10;
	do {
		lastDistance = currentDistance;
		minDM = NULL;
		for(d = 0; d < 4; ++d) {
			switch(d) {
				case 0 : dx = x + 1; dy = y; break;
				case 1 : dx = x - 1; dy = y; break;
				case 2 : dx = x; dy = y + 1; break;
				case 3 : dx = x; dy = y - 1; break;
			}
			currentElement = grille_case(grille, dx, dy);
			if(currentElement == NULL
			|| *currentElement == GRILLE_ITEM_WALL) {
				continue;
			}
			currentDM = grille_distance_case(grille, dx, dy);
			if(minDM == NULL || (currentDM != NULL && *currentDM < *minDM)) {
				min_dx = dx;
				min_dy = dy;
				minDM = currentDM;
			}
		}
		x = min_dx;
		y = min_dy;
		currentDistance = *minDM;
		if(currentDistance <= 0
		|| *grille_case(grille, x, y) == GRILLE_ITEM_EXIT) {
			break;
		}
		if(max(x - camera->x, y - camera->y) <= 1. * camera->distance) {
			
			/*lum = 1.1 - ((x - camera->x) * (x - camera->x) + (y - camera->y) * (y - camera->y)) / (0.8 * camera->distance * camera->distance);
			lum = (lum > 1) ? 1 : ((lum < 0) ? 0 : lum);*/
			lum = 1;
			if(camera->actions - camera->niveau * 10 > 0)
				displayRoundTile(camera, x - camera->x, y - camera->y, 0.25 * (min(max(camera->actions - camera->niveau * 10, 0), camera->niveau * 10) / (camera->niveau * 10.)) * steps / 10, lum * 50, lum * 50, lum * 25);
		}
		/*TODO : mvprintw(offset_y + y - camera->y, offset_x + x - camera->x, ".");*/
		--steps;
	} while(currentDistance < lastDistance && steps > 0);
	displayPerso(camera, (camera->niveau) % NB_PERSOS, 0, 0, 1., directionToPersoId(camera->dx, camera->dy));
	/*displayRoundTile(camera, 0, 0, 0.9, 50, 255, 50);*/
	/*TODO : mvprintw(offset_y, offset_x, "%c", GRILLE_ITEM_PLAYER);
	mvprintw(offset_y, offset_x, "");*/
	
}