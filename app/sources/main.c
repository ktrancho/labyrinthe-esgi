
#include "../includes/window.h"
#include "../includes/grille.h"
#include "../includes/jeu.h"

int main() {
	
	init_window();
	
	lancer_app();
	
	quit_window();
	
	exit(EXIT_SUCCESS);
}