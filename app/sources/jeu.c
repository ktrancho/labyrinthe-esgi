#include "../includes/jeu.h"

#include "../includes/window.h"
#include "../includes/camera.h"

int is_valid(int x, int y, Grille grille) {

	return (x >= 0 && y >= 0 && x < grille->largeur && y < grille->hauteur && 
	*grille_case(grille, x, y) != GRILLE_ITEM_WALL);
}

int is_finish(int x, int y, Grille grille) {

	return is_valid(x, y, grille) && 
	*grille_case(grille, x, y) == GRILLE_ITEM_EXIT;
}

int joueur_action(char action, int * x, int * y, int * dx, int * dy, Grille grille) {
	
	int move_x, move_y;
	int changement = 0;
	move_x = *x;
	move_y = *y;
	switch(action) {
		case 'z': --move_y; break;
		case 's': ++move_y; break;
		case 'q': --move_x; break;
		case 'd': ++move_x; break;
	}
	if((changement = (move_x != *x || move_y != *y))) {
		*dx = move_x - *x;
		*dy = move_y - *y;
	}
	if(changement && is_valid(move_x, move_y, grille)) {
		*x = move_x;
		*y = move_y;
	}
	return changement;
}

int choix_menu_principal() {
	int choix = 1;
	TTF_Font * font = NULL;
	int i;
	if((font = TTF_OpenFont("media/arial.ttf", 50)) == NULL) {
		fprintf(stderr, "In choix_menu_principal() : "
		"\n    ERROR : impossible d\'ouvrir media/arial.ttf\n");
		return choix;
	}
	SDL_Color textColor = {255, 255, 255};
	SDL_Rect pos;
	SDL_Event event;
	SDL_Surface * textChoix[] = {
		TTF_RenderUTF8_Blended(font, "Jouer", textColor),
		TTF_RenderUTF8_Blended(font, "Générer", textColor),
		TTF_RenderUTF8_Blended(font, "Quitter", textColor),
		NULL
	};
	
	for(;;) {
		SDL_FillRect(window_window.screen, NULL, SDL_MapRGB(window_window.screen->format, 0, 0, 0));
		pos.w = 25;
		pos.h = 25;
		pos.x = 50 + 12;
		pos.y = 50 + 100 * choix - 12;
		SDL_FillRect(window_window.screen, &pos, SDL_MapRGB(window_window.screen->format, 255, 255, 255));
		for(i = 0; textChoix[i] != NULL; ++i) {
			pos.w = 0;
			pos.h = 0;
			pos.x = 100;
			pos.y = 50 + 100 * (i + 1) - textChoix[i]->h / 2;
			SDL_BlitSurface(textChoix[i], NULL, window_window.screen, &pos);
		}
		SDL_Flip(window_window.screen);
		SDL_WaitEvent(&event);
		switch(event.type) {
			case SDL_QUIT : {
				choix = CHOIX_MENU_P_QUIT;
				goto madeChoice;
			} break;
			case SDL_KEYDOWN : {
				switch(event.key.keysym.sym) {
					case SDLK_UP : case SDLK_z : if(choix > 1) --choix; break;
					case SDLK_DOWN : case SDLK_s :  if(choix < CHOIX_MENU_P_QUIT) ++choix; break;
					case SDLK_RETURN : goto madeChoice;
					case SDLK_ESCAPE : {
						choix = CHOIX_MENU_P_QUIT;
						goto madeChoice;
					} break;
					default : break;
				}
			} break;
			default : break;
		}
	}
	
	madeChoice :;
	
	for(i = 0; textChoix[i] != NULL; ++i) {
		SDL_FreeSurface(textChoix[i]);
	}
	TTF_CloseFont(font);
	/* TODO : adapter avec SDL */
	#if 0
	for(;;) {
		clear();
		mvprintw(1, 1, "+-----------------+");
		mvprintw(2, 1, "| Labyrinthe-ESGI |");
		mvprintw(3, 1, "+-----------------+");
		mvprintw(3 + 2 * choix, 1, ">");
		mvprintw(3 + 2 * CHOIX_MENU_P_JOUER, 3, "Jouer");
		mvprintw(3 + 2 * CHOIX_MENU_P_GENERER, 3, "Générer");
		mvprintw(3 + 2 * CHOIX_MENU_P_QUIT, 3, "Quitter");
		refresh();
		switch(getch()) {
			case 'z': if(choix > 1) --choix; break;
			case 's': if(choix < CHOIX_MENU_P_QUIT) ++choix; break;
			case '\n' : return choix;
		}
	}
	#endif
	return choix;
}

char getAction(SDL_Event * event) {
	switch(event->type) {
		case SDL_KEYDOWN : {
			switch(event->key.keysym.sym) {
				case SDLK_z : return 'z';
				case SDLK_q : return 'q';
				case SDLK_s : return 's';
				case SDLK_d : return 'd';
				case SDLK_m : return 'm';
				case SDLK_UP : return 'z';
				case SDLK_DOWN : return 's';
				case SDLK_LEFT : return 'q';
				case SDLK_RIGHT : return 'd';
				case SDLK_ESCAPE : return 'm';
				default : break;
			} break;
		} break;
		case SDL_QUIT : return 'm';
		default : break;
	}
	return ' ';
}

void lancer_jeu() {

	int niveau = 0;
	int action;
	Grille grille = NULL;
	Camera camera;
	SDL_Event event;
	
	int x, y;
	int dx, dy;
	
	while((grille = charger_grille_niveau(niveau))) {
		fprintf(stderr, "Terrain nv %d : \n", niveau);
		afficher_grille_debug(stderr, grille);
		grille_trouver_item(grille, GRILLE_ITEM_PLAYER, &x, &y);
		*grille_case(grille, x, y) = GRILLE_ITEM_VOID;
		camera = creer_camera(x, y, dx, dy, 6, niveau, grille);
		dx = 0; dy = 1;
		do {
			SDL_FillRect(window_window.screen, NULL, SDL_MapRGB(window_window.screen->format, 0, 0, 0));
			camera_afficher(&camera);
			SDL_Flip(window_window.screen);
			SDL_WaitEvent(&event);
			camera.actions += joueur_action(action = getAction(&event), &x, &y, &dx, &dy, grille);
			camera_set_position(&camera, x, y, dx, dy);
			if(action == 'm') {
				goto end_game;
			}
		} while(! is_finish(x, y, grille));
		free_grille(&grille);
		++niveau;
	}
	
	end_game:;
	
	free_grille(&grille);
}

void generer_niveaux() {
	const int number = 100;
	char path[200];
	int i;
	Grille grille = NULL;
	int largeur, hauteur;
	for(i = 2; i < number; ++i) {
		largeur = 5 + i * 2 + rand() % 5;
		hauteur = 3 + i * 2 + rand() % 5;
		grille = generer_grille(
			largeur, hauteur
		);
		sprintf(path, "media/maps/nv%04d.laby", i);
		grille_save(grille, path);
		sprintf(path, "media/maps/nv%04d.laby_solving", i);
		grille_save_solving(grille, path);
		free_grille(&grille);
	}
}

void lancer_app() {
	/* TODO : remettre en place lorsqu'adapté avec SDL */
	#if 1
	int choix_menu;
	
	while((choix_menu = choix_menu_principal()) != CHOIX_MENU_P_QUIT) {
		switch(choix_menu) {
			/* jouer */
			case CHOIX_MENU_P_JOUER : {
				lancer_jeu();
			} break;
			
			case CHOIX_MENU_P_GENERER : {
				generer_niveaux();
			} break;
			
			default : break;
		}
	}
	#else
	lancer_jeu();
	#endif
}

Grille charger_grille_niveau(int id) {

	Grille grille = NULL;
	int largeur, hauteur;
	int has_dp = 0;
	char path[200];
	
	switch(id) {
		
		case 0 : {
			largeur = 5;
			hauteur = 3;
			grille = creer_copie_grille_texte(
				"#####"
				"#@  x"
				"#####",
				largeur, hauteur
			);
		} break;
		
		case 1 : {
			largeur = 7;
			hauteur = 5;
			grille = creer_copie_grille_texte(
				"#######"
				"#@#   #"
				"# # # #"
				"#   # #"
				"#####x#",
				largeur, hauteur
			);
		} break;
		
		default : {
			sprintf(path, "media/maps/nv%04d.laby", id);
			if((grille = grille_load(path)) != NULL) {
				break;
			}
			largeur = 5 + id * 2 + rand() % 5;
			hauteur = 3 + id * 2 + rand() % 5;
			grille = generer_grille(
				largeur, hauteur
			);
			has_dp = 1;
		} break;
		
		/*case 2 : {
			largeur = 11;
			hauteur = 7;
			grille = creer_copie_grille(
				"###########"
				"#     #   #"
				"# ## #  # #"
				"#  #   ####"
				"##  # #   #"
				"#@ ##   # #"
				"#########.#",
				largeur, hauteur
			);
		} break;
		
		case 3 : {
			largeur = 32;
			hauteur = 9;
			grille = creer_copie_grille(
				"#@##############################"
				"#      #    # #   #            #"
				"### # ## #    # # ### ##### ####"
				"#   #  # ####   #       #      #"
				"# #### # #   ####### ######## ##"
				"# #  # #   #       #    #      #"
				"# ## # ########### ###### ######"
				"#    #              #          #"
				"##############################.#",
				largeur, hauteur
			);
		} break;*/
	}
	
	if(! has_dp) {
		int x_fin, y_fin;
		grille_trouver_item(grille, GRILLE_ITEM_EXIT, &x_fin, &y_fin);
		grille_compute_distanceMap(grille, x_fin, y_fin);
	}
	
	return grille;
}