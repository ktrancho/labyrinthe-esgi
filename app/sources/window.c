#include "../includes/window.h"

#include <stdlib.h>
#include <time.h>

Window window_window;

void init_window() {
	
	window_window.width = 600;
	window_window.height = 600;
	
	if(SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		fprintf(stderr, "In init_window() : "
		"\n    ERROR : SDL error when window was initialized: %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}
	if(TTF_Init() == -1)
	{
		fprintf(stderr, "In init_window() : "
		"\n    ERROR : SDL_ttf error when window was intialized: %s\n", TTF_GetError());
		exit(EXIT_FAILURE);
	}
	if((window_window.screen = SDL_SetVideoMode(window_window.width, window_window.height, 32, SDL_HWSURFACE)) == NULL) {
		fprintf(stderr, "In init_window() : "
		"\n    ERROR : Screen setting failed\n");
		exit(EXIT_FAILURE);
	}
	SDL_WM_SetCaption("Labyrinthe ESGI 2022 (Exemple) - Kevin TRANCHO", NULL);
	
	SDL_EnableKeyRepeat(100, 100);
	
	int i;
	char path[500];
	for(i = 0; i < NB_DECORS; ++i) {
		sprintf(path, "media/decors/%d.png", i);
		if((window_window.decors[i] = IMG_Load(path)) == NULL) {
			fprintf(stderr, "In init_window() : "
			"\n    ERROR : SDL error while loading image \"%s\": %s\n", path, IMG_GetError());
		}
	}
	for(i = 0; i < NB_PERSOS; ++i) {
		sprintf(path, "media/perso/%d.png", i);
		if((window_window.persos[i] = IMG_Load(path)) == NULL) {
			fprintf(stderr, "In init_window() : "
			"\n    ERROR : SDL error while loading image \"%s\": %s\n", path, IMG_GetError());
		}
	}
	
	srand(time(NULL));
}

void quit_window() {

	int i;
	for(i = 0; i < NB_DECORS; ++i) {
		SDL_FreeSurface(window_window.decors[i]);
	}
	for(i = 0; i < NB_PERSOS; ++i) {
		SDL_FreeSurface(window_window.persos[i]);
	}
	SDL_FreeSurface(window_window.screen);
	TTF_Quit();
	SDL_Quit();
}