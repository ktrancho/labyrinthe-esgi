#include "../includes/grille.h"

#include "../includes/window.h"

#include <assert.h>

GrilleElement * grille_case(Grille grille, int x, int y) {
	
	if(! inside_area(x, y, grille->largeur, grille->hauteur)) {
		return NULL;
	}
	return grille->data + x + (long)y * grille->largeur;
}

long * grille_distance_case(Grille grille, int x, int y) {
	
	if(! inside_area(x, y, grille->largeur, grille->hauteur)) {
		return NULL;
	}
	return grille->distanceMap + x + (long)y * grille->largeur;
}

int inside_area(int x, int y, int largeur, int hauteur) {
	return x >= 0 && y >= 0 && x < largeur && y < hauteur;
}

Grille creer_grille(int largeur, int hauteur) {

	Grille grille = NULL;
	if((grille = (Grille)malloc(sizeof(struct Grille))) == NULL) {
		fprintf(stderr, "In creer_grille(%d, %d) :"
		"\n    ERROR : Allocation failed (main structure)\n", largeur, hauteur);
		exit(EXIT_FAILURE);
	}
	grille->data = NULL;
	if((grille->data = (GrilleElement *)malloc(sizeof(GrilleElement) * largeur * hauteur)) == NULL) {
		fprintf(stderr, "In creer_grille(%d, %d) :"
		"\n    ERROR : Allocation failed (data)\n", largeur, hauteur);
		free(grille);
		exit(EXIT_FAILURE);
	}
	if((grille->distanceMap = (long *)malloc(sizeof(long) * largeur * hauteur)) == NULL) {
		fprintf(stderr, "In creer_grille(%d, %d) :"
		"\n    ERROR : Allocation failed (distanceMap)\n", largeur, hauteur);
		free(grille->data);
		free(grille);
		exit(EXIT_FAILURE);
	}
	grille->largeur = largeur;
	grille->hauteur = hauteur;
	long i;
	for(i = 0; i < (long)largeur * hauteur; ++i) {
		grille->data[i] = GRILLE_ITEM_VOID;
	}
	for(i = 0; i < largeur; ++i) {
		*grille_case(grille, i, 0) = GRILLE_ITEM_WALL;
		*grille_case(grille, i, hauteur - 1) = GRILLE_ITEM_WALL;
	}
	for(i = 1; i < hauteur - 1; ++i) {
		*grille_case(grille, 0, i) = GRILLE_ITEM_WALL;
		*grille_case(grille, largeur - 1, i) = GRILLE_ITEM_WALL;
	}
	for(i = 0; i < (long)largeur * hauteur; ++i) {
		grille->distanceMap[i] = (long)largeur * hauteur + 1;
	}
	return grille;
}

void free_grille(Grille * grille) {
	
	if(grille == NULL || *grille == NULL) {
		return;
	}
	if((*grille)->data != NULL) {
		free((*grille)->data);
		(*grille)->data = NULL;
	}
	if((*grille)->distanceMap != NULL) {
		free((*grille)->distanceMap);
		(*grille)->distanceMap = NULL;
	}
	free(*grille);
	*grille = NULL;
}

void copier_grille(Grille destination, Grille source) {

	int i;
	assert(destination != NULL);
	assert(destination->largeur == source->largeur);
	assert(destination->hauteur == source->hauteur);
	for(i = 0; i < source->largeur * source->hauteur; ++i) {
		destination->data[i] = source->data[i];
	}
}

Grille creer_copie_grille(Grille source) {
	Grille grille = creer_grille(source->largeur, source->hauteur);
	copier_grille(grille, source);
	return grille;
}

void copier_grille_texte(Grille destination, char * source, int largeur, int hauteur) {
	int i;
	for(i = 0; i < largeur * hauteur; ++i) {
		destination->data[i] = source[i];
	}
}

Grille creer_copie_grille_texte(char * source, int largeur, int hauteur) {
	Grille grille = creer_grille(largeur, hauteur);
	copier_grille_texte(grille, source, largeur, hauteur);
	return grille;
}

int grille_trouver_item(Grille grille, GrilleElement item, int * rx, int * ry) {
	int x, y;
	for(y = 0; y < grille->hauteur; ++y) {
		for(x = 0; x < grille->largeur; ++x) {
			if(*grille_case(grille, x, y) == item) {
				*rx = x;
				*ry = y;
				return 1;
			}
		}
	}
	return 0;
}

void afficher_grille_debug(FILE * out, Grille grille) {

	int x, y;
	for(y = 0; y < grille->hauteur; ++y) {
		for(x = 0; x < grille->largeur; ++x) {
			fprintf(out, "%c", *grille_case(grille, x, y));
		}
		fprintf(out, "\n");
	}
}

static void creuse_grille_aleatoire(Grille grille, int x, int y) {
	
	GrilleElement * current;
	
	if(! inside_area(x, y, grille->largeur, grille->hauteur)
	|| *(current = grille_case(grille, x, y)) == GRILLE_ITEM_VOID) {
		return;
	}
	
	if(*current != GRILLE_ITEM_EXIT) {
		/* préserve la bordure intérieure du labyrinthe */
		if(! inside_area(x - 1, y - 1, grille->largeur - 2, grille->hauteur - 2)) {
			return;
		}
	}
	int i, j;
	int dx, dy;
	int nombre_liens_adjacence4 = 0;
	int nombre_liens_adjacence8 = 0;
	/* comptabilisation des cases vides adjacentes en diagonal */
	
	for(i = 0; i < 4; ++i) {
		dx = x + ((i % 2 == 0) ? -1 : 1);
		dy = y + (((i / 2) % 2 == 0) ? -1 : 1);
		if(! inside_area(dx, dy, grille->largeur, grille->hauteur)) {
			continue;
		}
		if(*grille_case(grille, dx, dy) == GRILLE_ITEM_VOID) {
			++nombre_liens_adjacence4;
		}
	}
	/* comptabilisation des cases vides voisines */
	
	for(i = -1; i <= 1; ++i) {
		for(j = -1; j <= 1; ++j) {
			if(i == 0 && j == 0) {
				continue;
			}
			dx = x + i;
			dy = y + j;
			if(! inside_area(dx, dy, grille->largeur, grille->hauteur)) {
				continue;
			}
			if(*grille_case(grille, dx, dy) == GRILLE_ITEM_VOID) {
				++nombre_liens_adjacence8;
			}
		}
	}
	/* limitation des libertés de déplacement : évite création de grandes zones vides */
	if(nombre_liens_adjacence4 > 1) {
		return;
	}
	/* cassage de formes trop régulières : évite création de longues lignes droites */
	if(nombre_liens_adjacence8 > 2) {
		return;
	}
	if(*current == GRILLE_ITEM_WALL) {
		*current = GRILLE_ITEM_VOID;
	}
	
	/* construction d'une permutation : ordre de coloration des zones vides */
	char permutation[4] = {-1, -1, -1, -1};
	for(i = 0; i < 4; ++i) {
		j = rand() % 4;
		while(permutation[j] != -1) {
			j = (j + 1) % 4;
		}
		permutation[j] = i;
	}
	
	/* coloration des cases adjacentes */
	for(i = 0; i < 4; ++i) {
		switch(permutation[i]) {
			case 0 : dx = x + 1; dy = y; break;
			case 1 : dx = x - 1; dy = y; break;
			case 2 : dx = x; dy = y + 1; break;
			case 3 : dx = x; dy = y - 1; break;
		}
		if(! inside_area(dx, dy, grille->largeur, grille->hauteur)
		|| *grille_case(grille, dx, dy) != GRILLE_ITEM_WALL) {
			continue;
		}
		creuse_grille_aleatoire(grille, dx, dy);
	}
}

static int place_joueur(Grille grille) {

	long maxId = -1;
	long i;
	for(i = 0; i < (long) grille->largeur * grille->hauteur; ++i) {
		if(grille->data[i] == GRILLE_ITEM_WALL) {
			continue;
		}
		if(maxId < 0 || grille->distanceMap[i] > grille->distanceMap[maxId]) {
			maxId = i;
		}
	}
	grille->data[maxId] = GRILLE_ITEM_PLAYER;
	return maxId >= 0;
}

Grille generer_grille(int largeur, int hauteur) {
	
	Grille grille = creer_grille(largeur, hauteur);
	
	int x, y;
	for(x = 0; x < largeur; ++x) {
		for(y = 0; y < hauteur; ++y) {
			*grille_case(grille, x, y) = GRILLE_ITEM_WALL;
		}
	}
	
	int x_fin, y_fin;
	if(rand() % 2) {
		
		x_fin = (rand() % 2) ? 0 : (largeur - 1);
		y_fin = 1 + rand() % (hauteur - 2);
		
	} else {
		
		y_fin = (rand() % 2) ? 0 : (hauteur - 1);
		x_fin = 1 + rand() % (largeur - 2);
		
	}
	*grille_case(grille, x_fin, y_fin) = GRILLE_ITEM_EXIT;
	creuse_grille_aleatoire(grille, x_fin, y_fin);
	
	grille_compute_distanceMap(grille, x_fin, y_fin);
	if(! place_joueur(grille)) {
		fprintf(stderr, "In generer_grille(%d, %d) :"
		"\n    ERROR : Placement joueur impossible\n", largeur, hauteur);
	}
	
	return grille;
}

void grille_compute_distanceMap(Grille grille, int x, int y) {
	
	long currentDistance = 0;
	long indexStart = 0;
	long indexEnd = 1;
	long end = 1;
	long * doneIndexes = NULL;
	long i;
	int dx, dy;
	int d;
	long * currentDM = NULL;
	
	if((doneIndexes = (long *)malloc(sizeof(long) * grille->largeur * grille->hauteur)) == NULL) {
		fprintf(stderr, "In grille_compute_distanceMap(grille : {%d, %d}, %d, %d) :"
		"\n    ERROR : Allocation failed (doneIndexes)\n", grille->largeur, grille->hauteur, x, y);
		exit(EXIT_FAILURE);
	}
	for(i = 0; i < (long) grille->largeur * grille->hauteur; ++i) {
		grille->distanceMap[i] = (long) grille->largeur * grille->hauteur + 1;
	}
	*doneIndexes = x + (long)y * grille->largeur;
	
	while(indexEnd > indexStart) {
		for(i = indexStart; i < indexEnd; ++i) {
			x = doneIndexes[i] % grille->largeur;
			y = doneIndexes[i] / grille->largeur;
			for(d = 0; d < 4; ++d) {
				switch(d) {
					case 0 : dx = x + 1; dy = y; break;
					case 1 : dx = x - 1; dy = y; break;
					case 2 : dx = x; dy = y + 1; break;
					case 3 : dx = x; dy = y - 1; break;
				}
				currentDM = grille_distance_case(grille, dx, dy);
				if(currentDM == NULL
				|| *grille_case(grille, dx, dy) == GRILLE_ITEM_WALL
				|| *currentDM <= currentDistance) {
					continue;
				}
				*currentDM = currentDistance + 1;
				doneIndexes[end++] = dx + (long)dy * grille->largeur;
			}
		}
		indexStart = indexEnd;
		indexEnd = end;
		++currentDistance;
	}
	
	free(doneIndexes);
	
}

int grille_save(Grille grille, const char * file_path) {
	FILE * file = NULL;
	int l, c;
	if((file = fopen(file_path, "w+")) == NULL) {
		fprintf(stderr, "In grille_save(grille : {%d, %d}, \"%s\") :"
		"\n    ERROR : Unable to open file\n", grille->largeur, grille->hauteur, file_path);
		return 0;
	}
	fprintf(file, "%d %d\n", grille->largeur, grille->hauteur);
	for(l = 0; l < grille->hauteur; ++l) {
		for(c = 0; c < grille->largeur; ++c) {
			fputc(*grille_case(grille, c, l), file);
		}
		fputc('\n', file);
	}
	fclose(file);
	return 1;
}

Grille grille_load(const char * file_path) {
	FILE * file = NULL;
	int largeur = 0, hauteur = 0;
	Grille grille = NULL;
	int l, c;
	if((file = fopen(file_path, "r")) == NULL) {
		return NULL;
	}
	if(fscanf(file, "%d %d\n", &largeur, &hauteur) != 2) {
		fprintf(stderr, "In grille_load(\"%s\") :"
		"\n    ERROR : Invalid reading in file\n", file_path);
		return NULL;
	}
	if((grille = creer_grille(largeur, hauteur)) == NULL) {
		fprintf(stderr, "In grille_load(\"%s\") :"
		"\n    ERROR : Invalid dimensions (%d, %d)\n", file_path, largeur, hauteur);
		return NULL;
	}
	for(l = 0; l < grille->hauteur; ++l) {
		for(c = 0; c < grille->largeur; ++c) {
			*grille_case(grille, c, l) = fgetc(file);
		}
		if(fgetc(file) != '\n') {
			fprintf(stderr, "In grille_load(\"%s\") -> (%d, %d) :"
			"\n    ERROR : Invalid end of line %d\n", file_path, largeur, hauteur, l);
		}
	}
	return grille;
}

static int charInStr(char c, const char * s) {
	for(; *s; ++s) if(c == *s) return 1;
	return 0;
}

int grille_save_solving(Grille grille, const char * file_path) {
	FILE * file = NULL;
	int l, c;
	if((file = fopen(file_path, "w+")) == NULL) {
		fprintf(stderr, "In grille_save(grille : {%d, %d}, \"%s\") :"
		"\n    ERROR : Unable to open file\n", grille->largeur, grille->hauteur, file_path);
		return 0;
	}
	int x, y;
	grille_trouver_item(grille, GRILLE_ITEM_PLAYER, &x, &y);
	int d;
	int dx, dy;
	int min_dx = x, min_dy = y;
	int last_x = x, last_y = y;
	GrilleElement * currentElement = NULL;
	long * currentDM = NULL;
	long * minDM = NULL;
	long lastDistance;
	long currentDistance = *grille_distance_case(grille, x, y);
	char dirView = '.';
	char minDirView = '.';
	do {
		lastDistance = currentDistance;
		minDM = NULL;
		for(d = 0; d < 4; ++d) {
			switch(d) {
				case 0 : dx = x + 1; dy = y; dirView = '>'; break;
				case 1 : dx = x - 1; dy = y; dirView = '<'; break;
				case 2 : dx = x; dy = y + 1; dirView = 'v'; break;
				case 3 : dx = x; dy = y - 1; dirView = '^'; break;
			}
			currentElement = grille_case(grille, dx, dy);
			if(currentElement == NULL
			|| *currentElement == GRILLE_ITEM_WALL) {
				continue;
			}
			currentDM = grille_distance_case(grille, dx, dy);
			if(minDM == NULL || (currentDM != NULL && *currentDM < *minDM)) {
				min_dx = dx;
				min_dy = dy;
				minDM = currentDM;
				minDirView = dirView;
			}
		}
		last_x = x;
		last_y = y;
		x = min_dx;
		y = min_dy;
		currentDistance = *minDM;
		if(currentDistance == 0
		|| *grille_case(grille, x, y) == GRILLE_ITEM_EXIT) {
			break;
		}
		if(*grille_case(grille, last_x, last_y) == GRILLE_ITEM_VOID) {
			*grille_case(grille, last_x, last_y) = minDirView;
		}
	} while(currentDistance < lastDistance);
	
	fprintf(file, "%d %d\n", grille->largeur, grille->hauteur);
	for(l = 0; l < grille->hauteur; ++l) {
		for(c = 0; c < grille->largeur; ++c) {
			currentElement = grille_case(grille, c, l);
			if(*currentElement == GRILLE_ITEM_WALL) {
				fputc('.', file);
			} else {
				fputc(*currentElement, file);
			}
			if(charInStr(*currentElement, "><v^")) {
				*currentElement = GRILLE_ITEM_VOID;
			}
		}
		fputc('\n', file);
	}
	fclose(file);
	return 1;
}