#ifndef DEF_HEADER_LABYESGI_WINDOW
#define DEF_HEADER_LABYESGI_WINDOW

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>

/* Module Window : */
/* Gère l'aspect fenêtre du jeu */
/* Module utilisant SDL */

#define NB_DECORS 4
#define NB_PERSOS 3

typedef struct Window Window;
struct Window {
	SDL_Surface * screen;
	int width;
	int height;
	SDL_Surface * decors[NB_DECORS];
	SDL_Surface * persos[NB_PERSOS];
};

extern Window window_window;

/* initialise les paramètres de la fenêtre */
void init_window();

/* termine proprement l'arrêt de la fenêtre */
void quit_window();

#endif