#ifndef DEF_HEADER_LABYESGI_GRILLE
#define DEF_HEADER_LABYESGI_GRILLE

#include <stdio.h>
#include <stdlib.h>

/* Module Grille : */
/* Éléments de base de construction et gestion de la grille d'un labyrinthe */

/* Élément présent sur une grille */
typedef enum {
	GRILLE_ITEM_WALL = '#',
	GRILLE_ITEM_VOID = ' ',
	GRILLE_ITEM_PLAYER = '@',
	GRILLE_ITEM_EXIT = 'x'
} GrilleElement;

/* Grille */
typedef struct Grille * Grille;
struct Grille {
	GrilleElement * data; /* ensemble des éléments présents sur la grille */
	int largeur; /* largeur de la grille */
	int hauteur; /* hauteur de la grille */
	long * distanceMap;
};

/* donne un pointeur sur une case de la grille */
GrilleElement * grille_case(Grille grille, int x, int y);

/* donne un pointeur sur une case de la carte des distances */
long * grille_distance_case(Grille grille, int x, int y);

/* renvoie vrai si les coordonnées (x, y) se trouvent dans la region (0, 0) - (largeur, hauteur) */
int inside_area(int x, int y, int largeur, int hauteur);

/* crée une grille de taille donnée */
Grille creer_grille(int largeur, int hauteur);

/* libère une grille */
void free_grille(Grille * grille);

/* copie une grille source dans une grille destination allouée avant passage */
void copier_grille(Grille destination, Grille source);

/* crée une grille comme une copie d'une grille existante */
Grille creer_copie_grille(Grille source);

/* copie une grille textuelle source dans une grille destination allouée avant passage */
void copier_grille_texte(Grille destination, char * source, int largeur, int hauteur);

/* crée une grille textuelle comme une copie d'une grille existante */
Grille creer_copie_grille_texte(char * source, int largeur, int hauteur);

/* renvoie les coordonnées du premier item trouvé dans la grille */
int grille_trouver_item(Grille grille, GrilleElement item, int * x, int * y);

/* affiche une grille donnée */
void afficher_grille_debug(FILE * out, Grille grille);

/* génère aléatoirement une grille de labyrinthe */
Grille generer_grille(int largeur, int hauteur);

/* fabrique la carte des distances à une position donnée */
void grille_compute_distanceMap(Grille grille, int x, int y);

/* sauvegarde une grille dans un fichier éditable à la main */
int grille_save(Grille grille, const char * file_path);

/* charge une grille dans un fichier éditable à la main */
Grille grille_load(const char * file_path);

/* sauvegarde un plan de la grille avec la résolution d'un chemin possible */
int grille_save_solving(Grille grille, const char * file_path);


#endif