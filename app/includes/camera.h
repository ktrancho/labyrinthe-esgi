#ifndef DEF_HEADER_LABYESGI_CAMERA
#define DEF_HEADER_LABYESGI_CAMERA

#include "grille.h"
#include "window.h"

/* Module Camera : */
/* Gère une caméra placée sur le terrain et l'affichage de celui-ci */

typedef struct Camera Camera;
struct Camera {
	int x;
	int y;
	int dx;
	int dy;
	int distance;
	int niveau;
	int actions;
	Grille looking_grille;
};

Camera creer_camera(int x, int y, int dx, int dy, int distance, int niveau, Grille grille);

void camera_set_position(Camera * camera, int x, int y, int dx, int dy);

void camera_afficher(Camera * camera);

#endif