#ifndef DEF_HEADER_LABYESGI_JEU
#define DEF_HEADER_LABYESGI_JEU

#include "grille.h"

/* Module Jeu : */
/* Gère le jeu et les événements / interactions qui le régissent */

typedef enum {
	CHOIX_MENU_P_JOUER = 1,
	CHOIX_MENU_P_GENERER = 2,
	CHOIX_MENU_P_QUIT
} ChoixMenuPrincipal;

/* is_valid vérifie que les coordonnées (x, y) sont valides pour un déplacement */
int is_valid(int x, int y, Grille grille);

/* is_finish vérifie que l'emplacement sur lequel se trouve les coordonnées (x, y) est une sortie */
int is_finish(int x, int y, Grille grille);

/* gère les actions du joueur : déplacement, renvoie 1 s'il s'est déplacé et 0 sinon */
int joueur_action(char action, int * x, int * y, int * dx, int * dy, Grille grille);

/* gère le menu principal */
int choix_menu_principal();

/* lance le jeu */
void lancer_jeu();

/* genere les niveaux en fichiers */
void generer_niveaux();

/* lance l'application */
void lancer_app();

/* charge le niveau dont l'id est indiqué */
Grille charger_grille_niveau(int id);

#endif