# Labyrinthe-ESGI

Projet de Kevin TRANCHO donné en première année de langage C à l'ESGI, Paris.
Ceci est un exemple de réalisation que pourrait produire un étudiant.
Toute recopie de ce projet même partielle en dehors de corrections vues en classe
passera la note de votre projet à zéro.

## Notation

Note actuelle : 24.5 / 20

### Aspects code :

| État | Points | Indication                                |
| :--: | :----: | :---------------------------------------- |
| [OK] | 0.5    | Le code compile                           |
| [OK] | 0.5    | Aucun warning                             |
| [OK] | 1      | Code indenté                              |
| [OK] | 1      | Conventions de nommage pertinentes        |
| [OK] | 1      | Découpe en fonctions pertinentes          |
| [OK] | 1      | Documentation (commentaires) pertinente   |
| [OK] | 1      | Rapport ou README.md                      |
|  6   | 6      | Total                                     |

### Aspects fonctionnalités :

| État | Points | Indication                              |
| :--: | :----: | :-------------------------------------- |
| [OK] | 1      | Pas de crash pendant exécution          |
| [OK] | 0.5    | Le code libère la mémoire               |
| [OK] | 0.5    | Menu                                    |
| [OK] | 0.5    | Affiche une grille                      |
| [OK] | 0.5    | Déplace le personnage                   |
| [OK] | 0.5    | Le personnage est bloqué par les murs   |
| [OK] | 0.5    | Le personnage ne sort pas de la carte   |
| [OK] | 0.5    | Termine le niveau                       |
| [OK] | 0.5    | Permet de rejouer                       |
| [OK] | 0.5    | Différentes tailles de carte            |
| [OK] | 0.5    | Niveau de labyrinthe (à la main)        |
|  6   | 6      | Total                                   |

### Améliorations :

| État | Points | Indication                                                     |
| :--: | :----: | :------------------------------------------------------------- |
| [OK] | 0.5    | Aucun warning avec -Wall                                       |
| [OK] | 0.5    | Séparation modulaire du code (.h)                              |
| [OK] | 0.5    | Un makefile est donné pour compiler                            |
| [OK] | 0.5    | Couleurs pertinentes                                           |
| [OK] | 1      | Niveaux supplémentaires                                        |
| [~~] | 1      | Éditeur de niveaux                                             |
| [OK] | 1      | Avec caméra fixée sur le joueur                                |
| [  ] | 1      | Adversaires (qui se déplacent vers le joueur pour le capturer) |
| [OK] | 1      | La machine indique un chemin à suivre pour sortir              |
| [OK] | 1      | Sous interface graphique (SDL) : vue de haut ou minicarte      |
| [  ] | 1      | En vue style 3D (SDL)                                          |
| [OK] | 1      | Gestion de versions avec git                                   |
| [OK] | 2      | Labyrinthe généré automatiquement                              |
| [OK] | 1      | Amélioration personnelle justifiée dans le rapport             |
|      |        | Code : Structure pour la grille et enum pour les items         |
| [OK] | 1      | Amélioration personnelle justifiée dans le rapport             |
|      |        | Fonctionnalité : carte de distance à l'arrivée                 |
| [OK] | 1      | Amélioration personnelle justifiée dans le rapport             |
|      |        | Fonctionnalité : sauvegarde et chargement de niveaux dans      |
|      |        | fichiers éditables                                             |
| 12.5 | 15     | Total                                                          |

## Compilation

Vous pouvez compiler le projet sous Linux à partir d'ici en utilisant 
les commandes suivantes :

```bash
make
./labyrintheESGI
```

## Améliorations personnelles

### Code : Structure pour la grille et enum pour les items

L'utilisation de la grille et son passage aux différentes fonctions demande le passage
redondant d'éléments qui y sont liés (largeur et hauteur de celle-ci). De plus, cet
aspect structure permet d'ajouter des éléments supplémentaires à la grille sans avoir
à modifier le prototype de chaque fonction qui utilise grille.

Ajout d'une enum pour gérer les items pour une modification plus simple de ceux-cis
(leur type si besoin ou le choix des caractères pour les items).
Attention cependant aux parties hardcodées comme les cartes à la main.

### Fonctionnalité : carte de distance à l'arrivée

Mise en place d'une carte de distance à l'arrivée (sortie du labyrinthe).
Cette carte de distance n'est calculée qu'une fois : à la génération du labyrinthe.
Elle est calculée comme un parcours en largeur dans le labyrinthe depuis des coordonnées
choisies.
Son utilisation dans le jeu est ensuite peu coûteuse (accès à un tableau) : chemin vers
la sortie, placement du joueur.

Ceci permet d'indiquer au joueur un chemin vers la sortie avec un calcul local de
celui-ci sur la partie visible à l'écran. Ceci permet de reproposer un chemin si le
joueur décide de ne pas suivre la recommandation ou s'il rejoint un autre chemin vers
l'arrivée.

La carte de distance permet de placer le joueur à plus grande distance de la sortie
lors de la génération automatique du niveau et éviter qu'il se retrouve trop proche
de la sortie.

La connaissance de la distance à la sortie permet de plus d'indiquer la longueur du
chemin qui mène à l'arrivée sans avoir à le calculer explicitement.

### Fonctionnalité : génération et édition de niveaux

Possibilité de générer des niveaux éditables dans des fichiers.
Un éditeur intégré au logiciel pourrait être proposé pour garantir que le niveau est 
réalisable ou assister le joueur.

## Bugs

### ncurses

L'utilisation de ncurses telle que donnée dans l'exercice 29 ne libère pas correctement
la mémoire en fin d'utilisation : `63,583 bytes in 102 blocks`
(Testé pour un code sans allocation dynamique directe de ma part).
Le code actuel ne libère pas la même quantité d'octets.

### Path finder

La proposition de chemin peut indiquer un bloc invalide au voisinage de la sortie.

